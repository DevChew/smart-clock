const { ipcRenderer } = require('electron');
const icons = require('./svgIcons');

let oldTemp = 0;
let oldPlace = '';

const formatWeather = (props) => {
    const { temp, place, istreshold } = props
    return(`<span class="${istreshold ? 'treshold' : ''}"><span class="city">${place}</span><span class="temperature">${temp} °C</span>${istreshold && icons.temperature || ''}</span>`);
}

ipcRenderer.on('weatherUpdate', (evemt, props) => {
    if( oldTemp != props.temp || oldTemp != props.place) {
        document.getElementById('weather').innerHTML = formatWeather(props);
        oldTemp = props.temp;
        oldTemp = props.place;
    }
})