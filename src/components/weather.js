// import metody umożliwiającej odpytywanie api
import fetch from 'node-fetch';

export default class Weather {

    // metoda wywoływana przy inicjalizacji pogody
    constructor(options) {

        // nazwa lokalizacji
        this.localizationName = options.localizationName || '';

        // klucz do api
        this.weatherApiKey = options.weatherApiKey || '';

        // adres do api rest'owego do pobierania aktualnej pogody
        this.weatherApiUri = 'https://api.openweathermap.org/data/2.5/weather?q=' + this.localizationName + '&units=metric&appid=' + this.weatherApiKey;
        
        // callback wywoływany przy aktualizacji pogody
        this.on_weatherUpdate = (data) => {};

        // interwał odświeżania pogody
        this.updateInterval = options.updateInterval || 10000;

        // stworzenie zmiennej zawierajacej informacje o interwale
        this.interval;

        // wywołanie metody 
        this.start();
    }

    // metoda umożliwiająca przypinanie eventów 
    on(event, callback) {
        // srawdzenie czy nazwa callbacku istnieje, jeśli tak to przypisanie do niej funkcji
        (this[`on_${event}`] && callback) ? this[`on_${event}`] = callback: false;
    }

    // odpytywanie api pogody a aktualną temperaturę
    updateWeather(weatherApiUri, callback) {

        // zapytanie api rest o pogodę
        fetch(weatherApiUri)

            // w momencie odpowiedzi
            .then((response) => {
                // przeformatuj ją do prztwarzalnego formatu
                return response.json();
            })

            // natępnie
            .then((jsonData) => {

                //przypisanie do zmiennej aktualnej temperatury dla danego miejsca
                const temp = jsonData.main.temp;

                // wywołanie metody określonej w wywołaniu tej metody 
                //oraz przekazanie jej informacji o aktualnej temperaturze
                callback({
                    temp: temp,
                    place: this.localizationName
                })
            })

            // w przypadku błędu, wypisz go do konsoli
            .catch((e) => {
                console.log('temp update error ' + e);
            });
    }

    // metoda start
    start() {

        //wywołanie zapytania o aktualną temperaturę
        this.updateWeather(this.weatherApiUri, this.on_weatherUpdate);

        // włącznie interwału
        this.interval = setInterval(() => {

            //wywołanie zapytania o aktualną temperaturę
            this.updateWeather(this.weatherApiUri, this.on_weatherUpdate)

        }, this.updateInterval);
    }

    // metoda umożliwiająca zatrzymanie odpytywania o pogodę
    stop() {

        // zatrzymanie interwału
        clearInterval(this.interval);
    }
}