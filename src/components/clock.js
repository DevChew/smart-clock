
const padTwo = (num) => {
    var s = num+"";
    while (s.length < 2) s = "0" + s;
    return s;
}

const formatClock = (date) => {
    const time = {
        hours: padTwo(date.getHours()),
        minutes: padTwo(date.getMinutes()),
        seconds: padTwo(date.getSeconds())
    }
    return (`<div class="clock"><span class="hours">${time.hours}</span><span class="minutes">${time.minutes}</span><span class="seconds">${time.seconds}</span></div>`);
};

const updateClock = () => {
    const clockWrapper = document.getElementById('clock');
    const date = new Date();
    clockWrapper.innerHTML = formatClock(date);
}

updateClock();

setInterval(updateClock, 1000);