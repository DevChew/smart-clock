const path = require('path');

document.addEventListener("DOMContentLoaded", function (event) {

    require(path.resolve(__dirname, './components/clock.js'));
    require(path.resolve(__dirname, './components/weather-front.js'));
    require(path.resolve(__dirname, './components/alarm-front.js'));
    require(path.resolve(__dirname, './components/options-front.js'));
    
});

const valueSpiner = (id, operation, linit) => {
    let newValue = document.getElementById(id).value;
    if (operation == 'add') {
        newValue++;
        if (newValue > linit) newValue = linit;
    };
    if (operation == 'substract') {
        newValue--;
        if (newValue < 0) newValue = 0;
    };

    document.getElementById(id).value = newValue;
}