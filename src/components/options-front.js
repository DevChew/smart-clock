const { ipcRenderer } = require('electron');

const padTwo = (num) => {
    var s = num+"";
    while (s.length < 2) s = "0" + s;
    return s;
}

let currentOptions = {
    alarmHours: 0,
    alarmMinutes: 0
}

const setOptions = (options) => {
    document.getElementById('alarm-hours').value = options.alarmHours;
    document.getElementById('alarm-minutes').value = options.alarmMinutes;
}

document.getElementById('show-options').addEventListener('click', () => {
    setOptions(currentOptions);
    document.getElementById('options').classList.add('active');
});

document.getElementById('cancel-options').addEventListener('click', () => {
    document.getElementById('options').classList.remove('active');
});

document.getElementById('save-options').addEventListener('click', () => {
    document.getElementById('options').classList.remove('active');
    
    const alarmHours = padTwo(document.getElementById('alarm-hours').value);
    const alarmMinutes = padTwo(document.getElementById('alarm-minutes').value);
    const tempTreshold = document.getElementById('temp-treshold').value;

    currentOptions.alarmHours = alarmHours;
    currentOptions.alarmMinutes = alarmMinutes;
    currentOptions.tempTreshold = tempTreshold;

    ipcRenderer.send('alarmSet', `${alarmHours}:${alarmMinutes}`);
    ipcRenderer.send('tempTresholdSet', tempTreshold);
});