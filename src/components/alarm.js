export default class Alarm {
    constructor(options) {

        // to trick umożliwi uruchomienie klasy bez podawania parametrów
        options = options ? options : {};

        // godzina budzenia w formacie hh:mm
        this.alarmTime = options.alarmTime || false; // in hh:mm format

        // dzień budzenia
        this.alarmDay = new Date().getDate(); // get the day of month

        // flaga oznaczająca przsunuecie alarmu
        this.alarmNeedToBeSooner = options.alarmNeedToBeSooner || false;

        // flaga oznaczająca stan alarmu (włączony / wyłączony)
        this.alarmEnabled = options.alarmEnabled || false;
        
        // interwal odświeżania sprawdzania godziny
        this.updateInterval = options.updateInterval || 1000;

        // o ile minut do tyłu ma być przestawiony alarm
        this.soonerTime = options.soonerTime || 10; // in minutes

        // callback wywoływany przy załaczeniu budzenia
        this.on_timeToWake = () => {};

        // callback wywoływany wyłączaniu alarmu
        this.on_alarmDisable = () => {};

        // callback wywoływany przy ustawianiu alarmu
        this.on_alarmSet = (alarmData) => {};
    }

    // metoda wywoływana cyklizcnie 
    tick() {

        // spradź czy alarm powinien zadzwonić
        if (this.isTimeToWakeUp()) {

            // wywołaj callback
            this.on_timeToWake();
            return true;
        }
        return false;
    }

    // metoda umożliwiająca przypinanie eventów 
    on(event, callback) {
        // srawdzenie czy nazwa callbacku istnieje, jeśli tak to przypisanie do niej funkcji
        (this[`on_${event}`] && callback) ? this[`on_${event}`] = callback: false;
    }

    setAlarm(alarmTime) {       

        // jesli nie podano czasu alarmu, nie rób nic
        if (!alarmTime) return false; 

        //zapisanie w zmiennej akttualnej daty i godziny
        const now = new Date();

        // stworzenie bardziej ustrukturyzowanych danych
        const alarmData = {

            // ustawienie dnia jako dzisiejdzy dzień
            day: now.getDate(),

            //czas z parametru wywołania metody
            time: alarmTime
        };

        //rozbicie czasu na minuty i godziny
        const [ hour, minutes ] = alarmTime.split(':');

        // zapisanie alarmu w postaci minut od godziny 00:00
        const alarmMinutes = parseInt(hour, 10) * 60 + parseInt(minutes, 10);

        // zapisanie aktualnego czasu w postaci minut od godziny 00:00
        const nowMinutes = parseInt(now.getHours(), 10) * 60 + parseInt(now.getMinutes(), 10);

        // jesli czas jest w rpzeszlosci, ustaw budzenie na jutro
        if ( alarmMinutes < nowMinutes ) alarmData.day++;

        //callback
        this.on_alarmSet(alarmData);

        // przypisanie nmowych wartości zmiennych
        this.alarmTime = alarmTime;
        this.alarmDay = alarmData.day;
        this.alarmEnabled = true;

        // wystartowanie interwału
        const alarmTick = setInterval(() => {

            // jeśli wybiła godzina budzenia, zatrzymaj interwał
            if (this.tick()) clearInterval(alarmTick);
        }, this.updateInterval);
    }


    // wyłączanie alarmu
    disableAlarm() {

        // ustaw flagę, aby alarm był wyłączony
        this.alarmEnabled = false;

        // wywołaj callback
        this.on_alarmDisable();
    }

    // ustaw flagę oznaczającą czy alarm powinien zadzwonić wcześniej
    setAlarmToBeSooner(value) {
        this.alarmNeedToBeSooner = value === false ? false : true ;
    }

    // metoda sprawdzajaca czy alarm powinen zadzwonić
    isTimeToWakeUp() {

        // pobierz aktualną datę i czas
        const now = new Date;

        // rozdziel czas alarmu na minuty i godzinę
        const alarm = !!this.alarmTime && this.alarmTime.split(':');

        // przypisz godzię do nowej zmiennej
        const alarmHour = alarm[0];

        // przypisz minuty do nowej zmiennej, sprawdzajac czy alarm powinen zadzownić wcześniej i odejmij o daną wartość
        const alarmMinutes = alarm[1] - (this.alarmNeedToBeSooner ? this.soonerTime : 0); // substract soonertime if needed

        // aby uprościć sprawdzanie czy nie powinnismy pzesunać godziny do tyłu, ustawiam czas budzenia, jako ilość minut od godziny 00:00
        
        const alrmInMunutes = (alarmHour * 60) + alarmMinutes;

        const nowInMinutes = (now.getHours() * 60) + now.getMinutes();

        // sprawdzenie czy alarm powinien zadzwonić dziś, oraz czy o danym czasie
        return alarm && (this.alarmDay <= now.getDate() && nowInMinutes >= alrmInMunutes);
    }

}