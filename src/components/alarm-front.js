// import funkcji do przesyłania eventów do i z widoku do procesu node
const { ipcRenderer } = require('electron');

// ikony
const icons = require('./svgIcons');

/**
 * stworzenie elementu audio zawierającego aktualny budzik oraz przypisanie go do zmiennej audio
 * w celu umożliwienia sterowania danym elementem
 */
const audio = new Audio('./alarms-sounds/simple.wav');


/**
 * sprawdzenie czy plik audio skończył się odtwarzać, a następnie
 * przewinięcie do go początku, Auto loop
 */
audio.addEventListener('ended', function() {
    this.currentTime = 0;
    this.play();
}, false);

/**
 * wyświetlanie aktualnego czasu budzenia na ekranie
 * @param {string} alarmTime aktualna godzina i minuty alarmu
 */
const updateAlarmIndicator = (alarmTime) => {
    // przypisywanie do zmiennej element, 
    //tagu html w widoku zawierającego informacje o aktualnej godzinie budzenia
    const element = document.getElementById('alarm-indicator');

    // nadpisanie zawartości elementu nową aktualną godziną budzenia
    element.innerHTML = alarmTime ? `<span>${alarmTime} ${icons.alarm}</span>` : '';
}

// nasłuhiwanie na event z procesu Node.js
ipcRenderer.on('alarmSet', (evemt, props) => {
    //wywołanie metody 
    updateAlarmIndicator(props.time);
})

// nasłuhiwanie na event z procesu Node.js
ipcRenderer.on('alarmRing', () => {
    //dodanie do tagu w widoku z id alarm, dodatkowej klasy active
    document.getElementById('alarm').classList.add('active');

    // wywołanie metody odtwarzającej dzwięk alarmu
    playSound()
})

//stworzenie metody setAlarm
const setAlarm = (time) => {

    //wysłanie do procesu Node eventu z informacą o godzinie alarmu
    ipcRenderer.send('alarmSet', time);
}

//stworzenie metody playSound
const playSound = () => {

    // wywołanie metody odtwarzającej zwięk
    // zdefiniowany wcześniej do zmiennej audio
    audio.play();
}

// stworzenie metody wyłączającej alarm
const stopSound = () => {

    // wywołanie metdy zatrzymującej dzwięk 
    audio.pause();

    // przewinięcie do początku 
    audio.currentTime = 0;
}

// nasłuchiwanie na kliknięcie w element z id disableAlarm
document.getElementById('disableAlarm').addEventListener('click', () => {

    //wysłanie do procesu node eventu
    ipcRenderer.send('alarm-disable');

    // usunięcie z elementu o id alarm klasy css active
    document.getElementById('alarm').classList.remove('active');

    // wywołanie metody aktualizacji informacji o godzienie budzenia
    // po wywołaniu usunie informacje o godzinie budzeni
    updateAlarmIndicator(false);

    //zatrzymanie odtwarzanie dzwięku
    stopSound();
})
