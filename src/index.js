// import bibliotek
const {
  app,
  BrowserWindow,
  ipcMain
} = require('electron');
const path = require('path');

// import alarmu i pogody
const Alarm = require('./components/alarm');
const Weather = require('./components/weather');

// inizjalizacja alarmu
const alarm = new Alarm.default();

//inicjalizacja pogody
const weather = new Weather.default({
  localizationName: 'Warsaw',
  weatherApiKey: '0cb87101ee6408100c72fafd5490b9dc'
});

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// zmienna przchowujaca informacje o widoku
let mainWindow;

// metoda tworząca okno
const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    frame: false,
    fullscreen: true,
  });

  // meły ekran, bez ramek
  mainWindow.maximize();
  mainWindow.setFullScreen(true)

  // załaduj do widoku plik index.html
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // obługa zamykania aplikacji
  mainWindow.on('closed', () => {
    // Jeślli zostanie wywołanie zamknięcie okna, usuń aktualny widok
    mainWindow = null;
  });
};

// uruchomienie głównego prcesu gdy proces będzie gotowy
app.on('ready', () => {

  // stworzenie okna
  createWindow();

  //Alarm

  //eventy z okna

  // obsługa eventu z widoku, ustawiania alarmu
  ipcMain.on('alarmSet', (event, props) => {
    
    // wywołaj metode ustawiającą godzinę alarmu
    alarm.setAlarm(props);
  });

  // obsługa eventu z widoku, wyłączania alarmu
  ipcMain.on('alarmDisabled', (event, props) => {

    // wywołaj metodę wyłączającą alarm
    alarm.disableAlarm();
  });


  //eventy z klasy alarm

  // obsługa eventu z Alarmu, gdy budzik powinien zadzwonić
  alarm.on('timeToWake', () => {

    // wypisz do konsoli informacje o budzeniu
    console.log('alarm should ring');

    // wyślij do widoku event 
    mainWindow.webContents.send('alarmRing');
  })

  // obsługa eventu z Alarmu, ustawianie alarmu
  alarm.on('alarmSet', (alarmData) => {

    // wydzel, godzinę i informacje o dniu budzenia 
    const { time, day } = alarmData;

    // wypisz do konsoli informacje o ustawionym alarmie
    console.log('alarm set to: ' + time + (day > new Date().getDate() ? ' tomorrow' : ' today' ) + ' day: ' + day );
    
    // wyślij do widoku informacje o ustawionym alarmie
    mainWindow.webContents.send('alarmSet', { time:time });
  })
  
  // obsługa eventu z Alarmu, wyłązanie alarmu
  alarm.on('alarmDisable', () => {
    
    // wypisz do konsoli informacje o wyłączonym alarmie
    console.log('alarmdisabled');
    
    // wyślij do widoku informacje o wyłączonym alarmie
    mainWindow.webContents.send('alarmDisabled');
  })

  //Pogoda

  // ustawienie zimennej przechwoującej temperaturę graniczną
  let treshold = 0;

  //obsługa eventu z widoku, ustawianie temperatury granicznej
  ipcMain.on('tempTresholdSet', (event, props) => {

    // przypisz nową wartość
    treshold = props;

    // wypisz now wartość do konsolo
    console.log('new temp treshold: ' + treshold);
  });

  //obsługa eventu z widoku, aktualizacja pogody oraz wysłanie iformacji o koniecznosci przestawienia alarmu
  weather.on('weatherUpdate', (data) => {

    // jesli tempe eratura jest poniżej granicznej
    if (data.temp <= treshold){

      //wypisz informacje do konsoli 
      console.log('temperature below ' + treshold);
      
      // dodaj informacje o temperaturze ponizej granicznej, w celu zaktualizowania informacji w widoku
      data.istreshold = true;

      // wyślij do alarmu informacje, zę powinien właczyć się wcześniej
      alarm.setAlarmToBeSooner();
    } else {

      // jeśli temperatura jest powyżej granicznej
      // wyślij informacje do alarmy aby nieprzstawiał godziny alarmu
      alarm.setAlarmToBeSooner(false);
    }

    // wyśli do widoku dane o pogodzie
    mainWindow.webContents.send('weatherUpdate', data);
  })

});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});